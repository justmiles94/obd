DIR=$(dirname "$(readlink -f "$0")")
cd $DIR
mkdir -p ${DIR}/archive
curl --silent --output /dev/null amazonaws.com
wifi_connected=$?
if [ $wifi_connected -eq 0 ]; then
    echo  uploading logs and data
    aws s3 cp *.log s3://obd-data-log-bucket
    if [ $? -eq 0 ]; then
        mv ${DIR}/*.log ${DIR}/archive/
    fi
    aws s3 cp *.data.json s3://obd-data-log-bucket
    if [ $? -eq 0 ]; then
        mv ${DIR}/*.data.json ${DIR}/archive/
    fi
fi
echo begging scan....
python3 ${DIR}/obd_runner.py
