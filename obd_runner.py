import logging, logging.handlers, sys,  obd, pprint
from datetime import datetime

for arg in sys.argv:
    if '=' in arg and not arg.endswith('='):
        if arg.startswith('--log-level'):
            level = arg.split('=')[1]
script_location = sys.path[0]
timestring = datetime.now().strftime('%Y-%m')
handler = logging.handlers.RotatingFileHandler(f'{script_location}/obd_{timestring}.log', maxBytes=2000000000, backupCount=50)
formatter = logging.Formatter(fmt='%(levelname)s %(asctime)-15s %(message)s')
handler.setFormatter(formatter)
root = logging.getLogger()
root.setLevel(logging.INFO)
root.addHandler(handler)
logging.info('logger setup...')

connection = obd.OBD()
if not connection.is_connected():
	quit()
file = open(f'{script_location}/obd_{timestring}.data.json', "a")
print("Protocol Name: " + connection.protocol_name())
commandList = [obd.commands.RPM, obd.commands.SPEED, obd.commands.FUEL_LEVEL, obd.commands.STATUS_DRIVE_CYCLE, obd.commands.FUEL_RATE, obd.commands.THROTTLE_POS, obd.commands.ENGINE_LOAD]
filter(None, commandList)
logging.info(f'Command List: {commandList}')
if not commandList:
	quit()
for command in commandList:
	if not obd.commands.has_command(command):
		del commandList[command]
		logging.error(f'Command doesn\'t exist: {command}')
response = connection.query(cmd)
output = pprint.pformat(vars(response))
file.write(output)
while True:
	for cmd in commandList:
		response = connection.query(cmd)
		output = ',' + pprint.pformat(vars(response))
		file.write(output)
