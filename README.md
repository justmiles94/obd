# Real-Time OBD Data Analysis Tool
## Runs on a raspberry pi since obd library requires it
### Task list:
- Onstart of pi
- start.sh script to run the following
- if connected to known wifi
    - if log files remain to be uploaded
        - upload files to s3
        - verify upload success and delete local logs and data collected
    - git pull master branch
- start obd scan script
- flush data to file at interval
- push log and data to s3 at interval from separate process (may require limiting log/data file size and use rolling solution)

### Future goals:
- use chef/docker/etc to configure service, logging, startup, and other environment stuff as needed
- s3select update statements to add to a remote file in s3
- process to agregate data and derive info over time.. consider cloudwatch metrics
